package slydepay.com.tonte.sampleslydepayintegration;

import android.app.Application;

/**
 * Created by Tonte on 6/15/17.
 */

public class App extends Application{

    public static String MERCHANT_KEY = "XXXXXXXXX";
    public static String EMAIL_OR_MOBILE_NUMBER = "XXXXX@gmail.com";
    public static String CALLBACK_URL = "XXXXXXXXX";

}
